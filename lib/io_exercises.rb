# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.

# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  guesses = 0
  won = false
  target_num = get_comp_num
  until won
    user_guess = get_user_num
    guesses += 1

    if target_num == user_guess
      puts "#{user_guess} is correct! You took #{guesses} guesses."
      won = true
    elsif target_num < user_guess
      puts "#{user_guess} was too high."
    elsif target_num > user_guess
      puts "#{user_guess} was too low."
    end
  end
end

def get_comp_num
  target_num = rand(1..100)

  target_num
end

def get_user_num
  puts "Please guess a number between 1 & 100:"
  user_guess = Integer(gets.chomp)

  user_guess
end



# Write file_shuffler program that: * prompts the user for a file name
# * reads that file * shuffles the lines * saves it to the file
# "{input_name}-shuffled.txt".

def shuffle_file
  puts "Please enter a full file name: "
  filename = gets.chomp

  file_base = filename.split('.')[0]
  file_ext = '.' + filename.split('.')[1]

  File.open("#{file_base}-shuffled#{file_ext}", "w") do |file|
    File.readlines(filename).shuffle.each do |line|
      file.print line
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  shuffle_file
end
